# nono
ncurses nonogram program. Mostly exists to be an illustrative example of a
program that uses libnono.

This will not integrate a lot of features.  Things that will not be integrated
are any history or progress, solution time, etc.  This only will work on one
puzzle at a time.

Nonograms are picture crossword puzzles (more famously known by the names
Griddlers and Picross).  There is plenty of information about them available on
Wikipedia's Nonogram page.

# Author
nono is Copyright (C) 2015 Taylor C. Richberger

This library is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.

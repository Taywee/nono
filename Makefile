CC=cc
DESTDIR=/usr
CFLAGS=-c -ansi -Wall -Wextra -pedantic -Wmissing-prototypes -Wstrict-prototypes -Wold-style-definition -std=c99 -MMD -MP -O2 `pkg-config --cflags ncursesw libnono`
LDFLAGS=-s `pkg-config --libs ncursesw libnono`
SOURCES=main.c
OBJECTS=$(SOURCES:.c=.o)
DEPENDENCIES=$(SOURCES:.c=.d)
EXECUTABLE=nono

.PHONY: all clean install uninstall

all: $(EXECUTABLE)

-include $(DEPENDENCIES)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

install: $(EXECUTABLE)
	install -d $(DESTDIR)/bin
	install $(EXECUTABLE) $(DESTDIR)/bin/

uninstall:
	-rm $(DESTDIR)/bin/$(EXECUTABLE)

clean :
	rm $(EXECUTABLE) $(OBJECTS) $(DEPENDENCIES)

%.o: %.c
	$(CC) $(CFLAGS) $< -o $@

/*
    nono:  A simple ncurses nonogram program
    Copyright (C) 2015 Taylor C. Richberger <taywee@gmx.com>

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#define _POSIX_SOURCE
#include <string.h>
#include <locale.h>
#include <stdbool.h>

#include <ncurses.h>
#include <nono.h>

void draw_puzzle(const struct nono_puzzle *grid, unsigned int width, unsigned int height, unsigned int cursorX, unsigned int cursorY);

#define getgrid(grid, width, height, x, y) grid[y * width + x]
#define setgrid(grid, width, height, x, y, value) grid[y * width + x] = value

int main(int argc, char **argv)
{
    setlocale(LC_ALL, "");
    int cursor[2] = {0};
    FILE *file = fopen(argv[1], "r");
    if (file)
    {
        if (!fseek(file, 0, SEEK_END))
        {
            const long size = ftell(file);
            char buffer[size];

            if (fseek(file, 0, SEEK_SET))
            {
                fputs("Could not set to end of file\n", stderr);
                return 1;
            }

            fread(buffer, sizeof(char), size, file);

            enum nono_error error;
            struct nono_puzzle *puzzle = nono_puzzle_load_ascii(buffer, &error, size, 'X', '\n');
            if (error == NONO_ERROR_NONE)
            {
                const nono_row_width_t width = nono_puzzle_width(puzzle);
                const nono_row_width_t height = nono_puzzle_height(puzzle);

                WINDOW *win = initscr();
                start_color();
                // Normal
                init_pair(1, COLOR_BLACK, COLOR_WHITE);
                // Cursor
                init_pair(2, COLOR_BLACK, COLOR_YELLOW);
                curs_set(0);
                noecho();
                raw();
                nonl();
                keypad(win, true);
                draw_puzzle(puzzle, width, height, cursor[0], cursor[1]);
                int ch = KEY_LEFT;

                do
                {
                    int prevx = cursor[0];
                    int prevy = cursor[1];

                    switch (ch)
                    {
                        case KEY_LEFT:
                            {
                                if (cursor[0] > 0)
                                {
                                    --cursor[0];
                                }
                                break;
                            }

                        case KEY_RIGHT:
                            {
                                if (cursor[0] < width - 1)
                                {
                                    ++cursor[0];
                                }
                                break;
                            }

                        case KEY_UP:
                            {
                                if (cursor[1] > 0)
                                {
                                    --cursor[1];
                                }
                                break;
                            }

                        case KEY_DOWN:
                            {
                                if (cursor[1] < height - 1)
                                {
                                    ++cursor[1];
                                }
                                break;
                            }

                        case 'x':
                            {
                                switch (nono_puzzle_progress(puzzle, prevx, prevy))
                                {
                                    case NONO_BLOCK_BLANK:
                                        {
                                            nono_puzzle_mark(puzzle, prevx, prevy, NONO_BLOCK_X);
                                            break;
                                        }
                                    default:
                                        {
                                            nono_puzzle_mark(puzzle, prevx, prevy, NONO_BLOCK_BLANK);
                                        }
                                }
                                break;
                            }

                        case 'r':
                            {
                                draw_puzzle(puzzle, width, height, cursor[0], cursor[1]);
                                break;
                            }

                        case KEY_ENTER:
                        case '\n':
                        case '\r':
                        case ' ':
                            {
                                switch (nono_puzzle_progress(puzzle, prevx, prevy))
                                {
                                    case NONO_BLOCK_BLANK:
                                        {
                                            nono_puzzle_mark(puzzle, prevx, prevy, NONO_BLOCK_MARK);
                                            break;
                                        }
                                    default:
                                        {
                                            nono_puzzle_mark(puzzle, prevx, prevy, NONO_BLOCK_BLANK);
                                        }
                                }
                                break;
                            }
                    }

                    int x = cursor[0];
                    int y = cursor[1];

                    attron(COLOR_PAIR(1));

                    switch (nono_puzzle_progress(puzzle, prevx, prevy))
                    {
                        case NONO_BLOCK_X:
                            {
                                mvaddstr(1 + prevy, 2 + 2 * prevx, "▏X");
                                break;
                            }
                        case NONO_BLOCK_MARK:
                            {
                                mvaddstr(1 + prevy, 2 + 2 * prevx, "█▉");
                                break;
                            }
                        default:
                            {
                                mvaddstr(1 + prevy, 2 + 2 * prevx, "▏ ");
                                break;
                            }
                    }

                    attron(COLOR_PAIR(2));

                    switch (nono_puzzle_progress(puzzle, x, y))
                    {
                        case NONO_BLOCK_X:
                            {
                                mvaddstr(1 + y, 2 + 2 * x, "▏X");
                                break;
                            }
                        case NONO_BLOCK_MARK:
                            {
                                mvaddstr(1 + y, 2 + 2 * x, "▒▒");
                                break;
                            }
                        default:
                            {
                                mvaddstr(1 + y, 2 + 2 * x, "▏ ");
                                break;
                            }
                    }
                    refresh();

                    if (nono_puzzle_solved(puzzle))
                    {
                        break;
                    }
                } while ((ch = getch()) != 'q');
                endwin();

                nono_puzzle_free(puzzle);
                return 0;
            }
        }
    }
}

void draw_puzzle(const struct nono_puzzle *puzzle, unsigned int width, unsigned int height, unsigned int cursorX, unsigned int cursorY)
{
    attron(COLOR_PAIR(1));
    mvaddstr(0, 1, "┌");
    mvaddstr(height + 1, 1, "└");
    mvaddstr(height + 1, width * 2 + 2, "┘");
    mvaddstr(0, width * 2 + 2, "┐");
    for (unsigned int y = 0; y < height; ++y)
    {
        mvaddstr(y + 1, 1, "│");
        mvaddstr(y + 1, width * 2 + 2, "│");
    }
    for (unsigned int x = 0; x < width; ++x)
    {
        mvaddstr(0, 2 * x + 2, "──");
        mvaddstr(height + 1, 2 * x + 2, "──");
    }

    for (unsigned int y = 0; y < height; ++y)
    {
        for (unsigned int x = 0; x < width; ++x)
        {
            char *block = "█▉";
            if (x == cursorX && y == cursorY)
            {
                block = "▒▒";
                attron(COLOR_PAIR(2));
            } else
            {
                attron(COLOR_PAIR(1));
            }
            switch (nono_puzzle_progress(puzzle, x, y))
            {
                case NONO_BLOCK_X:
                    {
                        mvaddstr(1 + y, 2 + 2 * x, "▏X");
                        break;
                    }
                case NONO_BLOCK_MARK:
                    {
                        mvaddstr(1 + y, 2 + 2 * x, block);
                        break;
                    }
                default:
                    {
                        mvaddstr(1 + y, 2 + 2 * x, "▏ ");
                        break;
                    }
            }
        }
    }
}
